import random
import itertools
import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage


class BloodCounter:
    """Counter that counts red blood cells and platelets from a grayscale image over a clear ambient background."""

    def __init__(self, blood_image):
        """
        Initialize blood counter object for a given grayscale image of blood cells.

        :param np.ndarray blood_image: grayscale image of red blood cells and platelets (small dots) in a clear ambient.
        """

        # Save blood image
        self.image = blood_image

        # Initialize placeholders for segmented and annotated blood count images.
        self.image_segmented_noisy = None
        self.image_segmented = None
        self.image_count = None

        # Initialize histogram and detected bands placeholders.
        self.histogram = None
        self.bands = None

        # Set colors for each segments
        self.colors = [
            (128, 0, 128),  # Purple for platelets (histogram "background").
            (0, 0, 255),  # Red for red blood cells ("band 0" in the histogram).
            (0, 0, 0)  # Black for ambient ("band 1" in the histogram ).
        ]

        # Initialize red blood cells and pallets boolean masks.
        self.cells = None
        self.platelets = None

        # Initialize blood count results dictionaries.
        self.count = {}
        self.diameter = {}

    def __repr__(self):
        text = \
            f"{self.count['cells']} cells (avg. diameter {self.diameter['cells']:.1f})\n" \
            f"{self.count['platelets']} platelets (avg. diameter {self.diameter['platelets']:.1f})"
        return text

    def run(self):
        if not self.bands:
            self._detect_histogram_bands()
        self._segment_image()
        self._count_cells()

    def _detect_histogram_bands(self):
        """Detect and save bands corresponding to separate peaks in the grayscale histogram of the saved image."""

        # Calculate histogram on median-filtered grayscale image.
        filtered_image = ndimage.median_filter(self.image, size=3)
        histogram, _ = np.histogram(filtered_image, bins=256, range=(0, 255))

        # Detect separated peaks in the pixel intensity distribution using a relative threshold.
        threshold = 1 / 15
        max_count = np.max(histogram)
        grayscale_levels = np.where(np.diff(histogram > max_count * threshold))[0]

        # Convert detected grayscale levels into bands for each peak (make sure tuples contain type int).
        bands = [(int(a), int(b)) for a, b in zip(grayscale_levels[::2], grayscale_levels[1::2])]

        # Calculate midpoints between the detected bands (of type int).
        midpoints = []
        for this_band, next_band in zip(bands, bands[1:]):
            midpoints.append((this_band[1] + next_band[0]) // 2)

        # Expand the bands by extending each bend to the midpoints.
        for index, midpoint in enumerate(midpoints):
            bands[index] = (bands[index][0], midpoint)
            bands[index + 1] = (midpoint, bands[index + 1][1])

        # Save detected bands and calculated histogram.
        self.bands = bands
        self.histogram = histogram

        # If there is a mismatch between number of bands and colors randomly assign colors to bands (and background).
        if len(self.colors[1:]) != len(self.bands):
            self.colors = [(
                random.randint(0, 256),
                random.randint(0, 256),
                random.randint(0, 256)
            ) for _ in range(len(self.bands) + 1)]  # Color at index 0 is background.

    def _segment_image(self):
        """Segment the grayscale blood image into masks corresponding to cells, platelets and ambient."""

        # Define boolean masks for each detected band in the histogram (should be "cells" and "ambient").
        masks = [cv2.inRange(self.image, *band) for band in self.bands]

        # Combine detected bands into a color-coded image and save it: histogram-background + band0 + band1 + ...
        self.image_segmented_noisy = BloodCounter.combine_masks(
            masks=[BloodCounter.complement(masks), *masks],
            colors=self.colors
        )

        # Give meaningful names to detected bands: "platelets" (complement) + "cells" + "ambient".
        mask_cells, mask_ambient = masks

        # Clean ambient mask using binary closing (removes "hole-speckles" in the ambient).
        mask_ambient = ndimage.binary_closing(mask_ambient, structure=np.ones(shape=(10, 10))).astype(np.uint8)

        # Remove speckles from cells mask using binary opening.
        mask_cells = ndimage.binary_opening(mask_cells, structure=np.ones(shape=(12, 12))).astype(np.uint8)

        # Dilate cells mask and intersect it with the ambient complementary to correct size: cells mask is now ready.
        white = 255 * np.ones(shape=(*self.image.shape, 3), dtype=np.uint8)
        mask_cells = ndimage.binary_dilation(mask_cells, structure=np.ones(shape=(6, 6))).astype(np.uint8)
        mask_cells = cv2.bitwise_and(
            cv2.bitwise_and(white, white, mask=mask_cells),
            cv2.bitwise_and(white, white, mask=mask_cells),
            mask=cv2.bitwise_not(mask_ambient)
        )[:, :, 0]

        # Get the platelets mask by taking the difference between the clean ambient and cells masks.
        mask_palettes = cv2.bitwise_and(
            cv2.bitwise_not(cv2.bitwise_and(white, white, mask=mask_ambient)),
            cv2.bitwise_not(cv2.bitwise_and(white, white, mask=mask_ambient)),
            mask=cv2.bitwise_not(mask_cells)
        )[:, :, 0]

        # Remove speckles from platelets mask: platelets mask is now ready.
        mask_palettes = ndimage.binary_opening(mask_palettes, structure=np.ones(shape=(8, 8))).astype(np.uint8)

        # Combine mutually exclusive segments into color-coded image: "platelets" + "cells" + "ambient" (complement).
        masks = [mask_palettes, mask_cells]
        self.cells, self.platelets = reversed(masks)
        self.image_segmented = BloodCounter.combine_masks(
            masks=[*masks, BloodCounter.complement(masks)],
            colors=self.colors
        )

    def _count_cells(self):
        """Count red cells and platelets in the blood image."""

        # Count red blood cells in segmented image.
        num_labels, _, stats, centroids1 = cv2.connectedComponentsWithStats(self.cells, connectivity=4)
        self.count["cells"] = num_labels - 1  # Label 0 is background.
        self.diameter["cells"] = 2 * np.median(np.sqrt(stats[:, cv2.CC_STAT_AREA] / np.pi))  # Less sensitive to errors.

        # Count platelets in segmented image.
        num_labels, _, stats, centroids2 = cv2.connectedComponentsWithStats(self.platelets, connectivity=4)
        self.count["platelets"] = num_labels - 1
        self.diameter["platelets"] = 2 * np.median(np.sqrt(stats[:, cv2.CC_STAT_AREA] / np.pi))

        # Retrieve copies of the blood image and its segmented version.
        image_blood = cv2.cvtColor(self.image, cv2.COLOR_GRAY2BGR)
        image_segmented = self.image_segmented.copy()

        # Annotate the blood image with the count results.
        for text, location in zip(self.__repr__().split("\n"), [(50, 100), (50, 200)]):
            cv2.putText(
                image_blood,
                text,
                location,
                fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                fontScale=2,
                color=(0, 0, 0),
                thickness=6,
                lineType=cv2.LINE_AA
            )

        # Mark the counted cell centroids on the segmented image.
        for x, y in itertools.chain(centroids1, centroids2):
            cv2.circle(image_segmented, (int(x), int(y)), radius=2, color=(255, 255, 255), thickness=8)

        # Save side-by-side of both images as the final blood count result.
        self.image_count = np.hstack((image_blood, image_segmented))

    def plot_histogram_bands(self):
        """Plot the histogram of the saved grayscale image and detected bands corresponding to separate peaks."""

        # If no bands were detected - run detection.
        if not self.bands:
            self._detect_histogram_bands()

        # Plot histogram.
        fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(7, 7))
        ax.plot(self.histogram, "b", label="Histogram")

        # Color-code detected bands (convert BGR to RGB colors).
        alpha = 0.5
        for index, (this_band, color) in enumerate(zip(self.bands, self.colors[1:])):  # Color at index 0 is background.
            color = [channel / 255 for channel in reversed(color)]
            ax.axvspan(*this_band, color=(*color, alpha), label=f"Band {index}", ec="black", ls="--", lw=0.5)

        # Annotate figure.
        ax.legend()
        ax.set_title("Grayscale histogram and detected bands")
        plt.show(block=False)

    def plot_results(self):
        """Show the blood count result."""
        if not self.image_count:
            self.run()
        cv2.imshow("blood count", self.image_count)
        cv2.waitKey()
        cv2.destroyAllWindows()

    @staticmethod
    def complement(masks):
        """
        Create the complementary mask to the union of input masks.

        :param list masks: list of boolean masks of the same shape.
        :return np.ndarray: boolean mask that is the complementary of the union of input masks.
        """
        union_mask = cv2.inRange(masks[0], 1, 0)  # An all-black mask.
        for mask in masks:
            union_mask = cv2.bitwise_or(union_mask, mask)
        return cv2.bitwise_not(union_mask)

    @staticmethod
    def combine_masks(masks, colors):
        """
        Create a segmented image from a list of masks and corresponding colors where each mask is one segment.

        :param list masks: list of boolean masks corresponding to image segments.
        :param list colors: list of BGR colors (one per mask).
        :return np.ndarray: color-coded segmented image.
        """

        # Create a colored mask for each boolean mask.
        colored_bands = []
        for mask, color in zip(masks, colors):
            color = np.array(color, dtype=np.uint8)
            monochrome = np.tile(color[np.newaxis, np.newaxis, :], (*mask.shape, 1))  # Uniform tile..
            colored_bands.append(cv2.bitwise_and(monochrome, monochrome, mask=mask))

        # Combine colored masks into a segmented image.
        segmented_image = np.zeros(shape=(*masks[0].shape, 3), dtype=np.uint8)
        for colored_band in colored_bands:
            segmented_image += colored_band

        # Return segmented image.
        return segmented_image
