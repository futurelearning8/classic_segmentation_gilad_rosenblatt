import cv2
import matplotlib.pyplot as plt
from blood_count import BloodCounter


def main():
    # Load blood image from file.
    filename = "../images/platelets_q.jpg"
    image = cv2.imread(filename)

    # Create a blood counter object and provide it with the grayscale blood image.
    counter = BloodCounter(blood_image=cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))

    # Plot the histogram and detected grayscale bands.
    counter.plot_histogram_bands()
    plt.savefig("../images/histogram.png")
    plt.show()

    # Run and report blood count results.
    counter.plot_results()
    cv2.imwrite("../images/segmented_image_noisy.png", counter.image_segmented_noisy)
    cv2.imwrite("../images/segmented_image.png", counter.image_segmented)
    cv2.imwrite("../images/blood_count.png", cv2.resize(counter.image_count, (5120//2, 1920//2)))

    # Report to console.
    print(counter)


if __name__ == "__main__":
    main()
