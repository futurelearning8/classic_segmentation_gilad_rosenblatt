# Image Segmentation

This is the image segmentation exercise for FL8 week 4 day 3 by Gilad Rosenblatt.

## Instructions

### Run

Run `main.py` from `code` as current working directory to present the images.

### Data

Code accesses the `platelets_q.jpg` file from the `images` folder.

## Results

Below is a side-by-side comparison of the original image and the 3-part segmentation (red blood cells, platelets and ambient).

![segmentation](/images/blood_count_sbs.png?raw=true).

The segmentation was done by dividing the grayscale histogram shown below into two bands, and joining with the remaining range to form 3 masks. The peak bands correspond to red blood cells and the ambient, whereas the background contains information relating to the platelets.

![histogram](/images/histogram.png?raw=true).

After morphological filtering, connected components were extracted for the red blood cells and platelets masks. The component centroid points are marked on the image below. Counts and average diameters for each appear as annotations.

![count](/images/blood_count.png?raw=true).

## License

[WTFPL](http://www.wtfpl.net/)
